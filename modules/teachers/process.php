<?php
session_start();


require_once "../../config/database.php";

if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=index.php?alert=1'>";
}

else {
    if ($_GET['act']=='insert') {
        if (isset($_POST['Guardar'])) {

          $identificacion=mysqli_real_escape_string($mysqli, trim($_POST['id']));
          $nombre=mysqli_real_escape_string($mysqli, trim($_POST['nombre']));
          $apellidos=mysqli_real_escape_string($mysqli, trim($_POST['apellidos']));
          $email=mysqli_real_escape_string($mysqli, trim($_POST['email']));
          $direccion=mysqli_real_escape_string($mysqli, trim($_POST['direccion']));
          $telefono=mysqli_real_escape_string($mysqli, trim($_POST['telefono']));
          $estado=mysqli_real_escape_string($mysqli, trim($_POST['estado']));

          if($_FILES['foto']['name'] != ""){
          if ($_FILES['foto']['type'] == "image/jpeg" || $_FILES['foto']['type'] == "image/png" || $_FILES["foto"]["type"]=="image/pjpeg") {
            if ($_FILES['foto']['size'] <= 2000000) {
              $directorio = $_SERVER['DOCUMENT_ROOT'].'/biotek/images/docentes/';
              $foto = time().'_'.$_FILES['foto']['name'];
              move_uploaded_file($_FILES['foto']['tmp_name'], $directorio.$foto);
              $query = mysqli_query($mysqli, "INSERT INTO docentes(foto,identificacion,nombre,apellidos,email,direccion,telefono,estado)
                                              VALUES('$foto','$identificacion', '$nombre', '$apellidos', '$email', '$direccion', '$telefono','$estado')")
                                              or die('error '.mysqli_error($mysqli));

              if ($query) {

                  header("location: ../../main.php?module=teachers&alert=1");
              }
            }else{
                header("location: ../../main.php?module=form_teachers&form=add&alert=5");
            }
          }else{
            header("location: ../../main.php?module=form_teachers&form=add&alert=6");
          }
        }
      }
    }

    elseif ($_GET['act']=='update') {
        if (isset($_POST['Guardar'])) {
            if (isset($_POST['codigo'])) {

              $codigo=mysqli_real_escape_string($mysqli, trim($_POST['codigo']));
              $identificacion=mysqli_real_escape_string($mysqli, trim($_POST['id']));
              $nombre=mysqli_real_escape_string($mysqli, trim($_POST['nombre']));
              $apellidos=mysqli_real_escape_string($mysqli, trim($_POST['apellidos']));
              $email=mysqli_real_escape_string($mysqli, trim($_POST['email']));
              $direccion=mysqli_real_escape_string($mysqli, trim($_POST['direccion']));
              $telefono=mysqli_real_escape_string($mysqli, trim($_POST['telefono']));
              $estado=mysqli_real_escape_string($mysqli, trim($_POST['estado']));

              if($_FILES['foto']['name'] != ""){
              if ($_FILES['foto']['type'] == "image/jpeg" || $_FILES['foto']['type'] == "image/png" || $_FILES["foto"]["type"]=="image/pjpeg") {
                if ($_FILES['foto']['size'] <= 2000000) {
                  $directorio = $_SERVER['DOCUMENT_ROOT'].'/biotek/images/docentes/';
                  $foto_old = mysqli_real_escape_string($mysqli, trim($_POST['imagen']));
                  unlink($directorio.$foto_old);

                  $foto = time().'_'.$_FILES['foto']['name'];
                  move_uploaded_file($_FILES['foto']['tmp_name'], $directorio.$foto);
                  $query = mysqli_query($mysqli, "UPDATE docentes SET foto                  = '$foto',
                                                                      identificacion        = '$identificacion',
                                                                      nombre                = '$nombre',
                                                                      apellidos             = '$apellidos',
                                                                      email                 = '$email',
                                                                      direccion             = '$direccion',
                                                                      telefono              = '$telefono',
                                                                      estado                = '$estado'
                                                                WHERE id= '$codigo'")
                                                  or die('error: '.mysqli_error($mysqli));


                  if ($query) {

                      header("location: ../../main.php?module=teachers&alert=2");
                  }
                }else{
                    header("location: ../../main.php?module=form_teachers&form=edit&id=$identificacion&alert=5");
                }
              }else{
                header("location: ../../main.php?module=form_teachers&form=edit&id=$identificacion&alert=6");
              }
            }else{
              $foto = mysqli_real_escape_string($mysqli, trim($_POST['imagen']));
              $query = mysqli_query($mysqli, "UPDATE docentes SET foto                  = '$foto',
                                                                  identificacion        = '$identificacion',
                                                                  nombre                = '$nombre',
                                                                  apellidos             = '$apellidos',
                                                                  email                 = '$email',
                                                                  direccion             = '$direccion',
                                                                  telefono              = '$telefono',
                                                                  estado                = '$estado'
                                                            WHERE id= '$codigo'")
                                              or die('error: '.mysqli_error($mysqli));


              if ($query) {

                  header("location: ../../main.php?module=teachers&alert=2");
              }
            }
          }
        }
    }


    elseif ($_GET['act']=='enable') {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $estado = "habilitado";

            $query = mysqli_query($mysqli, "UPDATE docentes SET  estado = '$estado'
                                                          WHERE identificacion = '$id'")
                                            or die('error '.mysqli_error($mysqli));


            if ($query) {

                header("location: ../../main.php?module=teachers&alert=3");
            }
        }
    }

    elseif ($_GET['act']=='disable') {
        if (isset($_GET['id'])) {
          $id = $_GET['id'];
          $estado = "deshabilitado";

          $query = mysqli_query($mysqli, "UPDATE docentes SET  estado = '$estado'
                                                        WHERE identificacion = '$id'")
                                          or die('error '.mysqli_error($mysqli));


            if ($query) {

                header("location: ../../main.php?module=teachers&alert=4");
            }
        }
    }
}
?>
