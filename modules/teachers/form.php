<?php

if ($_GET['form']=='add') { ?>

 <section class="content-header">
   <h1>
     <i class="fa fa-book icon-title"></i> Agregar Docentes
   </h1>
   <ol class="breadcrumb">
     <li><a href="?module=start"><i class="fa fa-home"></i> Inicio </a></li>
     <li><a href="?module=courses"> Docentes </a></li>
     <li class="active"> Más </li>
   </ol>
 </section>

 <!-- Main content -->
 <section class="content">
   <div class="row">
     <div class="col-md-12">
       <div class="box box-primary">
         <!-- form start -->
         <form role="form" class="form-horizontal" action="modules/teachers/process.php?act=insert" method="POST" enctype="multipart/form-data">
           <div class="box-body">
              <?php
              if (isset($_GET['alert'])) {
                if($_GET['alert'] == 5){
                echo "<div class='alert alert-danger alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                        <h4>  <i class='icon fa fa-exclamation'></i> Error!</h4>
                      La imagen debe ser m&aacute;ximo de 2mb
                      </div>";
                }
              }
              if (isset($_GET['alert'])) {
                if($_GET['alert'] == 6){
                echo "<div class='alert alert-danger alert-dismissable'>
                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                        <h4>  <i class='icon fa fa-exclamation'></i> Error!</h4>
                      Debe subir una imagen válida
                      </div>";
                }
              }
               ?>
               <div class="form-group text-center">
                 <label style="color:red"><strong>* (Todos los campos son obligatorios)</strong></label>
               </div>
             <div class="form-group">
               <label class="col-sm-2 control-label">Identificaci&oacute;n <strong style="color:red">*</strong></label>
               <div class="col-sm-5">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-id-card" aria-hidden="true"></i></span>
                  <input type="text" class="form-control" name="id" id="id" required
                         placeholder="Cédula de extranjer&iacute;a / Cédula de ciudadan&iacute;a"
                         title="Ingrese Cédula de extranjer&iacute;a &oacute; Cédula de ciudadan&iacute;a"/>
                </div>
               </div>
             </div>

             <div class="form-group">
               <label class="col-sm-2 control-label">Foto <strong style="color:red">*</strong></label>
               <div class="col-sm-5">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-upload" aria-hidden="true"></i>&nbsp;</span>
                  <input type="file" name="foto" required
                         title="Seleccione foto de docente"/>
                </div>
               </div>
             </div>

             <div class="form-group">
               <label class="col-sm-2 control-label">Nombre <strong style="color:red">*</strong></label>
               <div class="col-sm-5">
                 <div class="input-group">
                   <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									 <input type="text" class="form-control" name="nombre" id="nombre" required placeholder="Nombre del docente"/>
                </div>
               </div>
             </div>

             <div class="form-group">
               <label class="col-sm-2 control-label">Apellidos <strong style="color:red">*</strong></label>
               <div class="col-sm-5">
                 <div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" required name="apellidos" id="apellidos"  placeholder="Apellidos del docente"/>
								</div>
               </div>
             </div>

             <div class="form-group">
               <label class="col-sm-2 control-label">E-mail <strong style="color:red">*</strong></label>
               <div class="col-sm-5">
                 <div class="input-group">
                   <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
 									<input type="email" class="form-control" required name="email" id="email"  placeholder="Ingrese correo electr&oacute;nico del docente"/>
								</div>
               </div>
             </div>

             <div class="form-group">
               <label class="col-sm-2 control-label">Direcci&oacute;n <strong style="color:red">*</strong></label>
               <div class="col-sm-5">
                 <div class="input-group">
                   <span class="input-group-addon"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>
									<textarea class="form-control" required name="direccion" id="direccion"  placeholder="Ingrese direcci&oacute;n del docente"></textarea>
                </div>
               </div>
             </div>

             <div class="form-group">
               <label class="col-sm-2 control-label">Tel&eacute;fono <strong style="color:red">*</strong></label>
               <div class="col-sm-5">
                 <div class="input-group">
                   <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
									<input type="number" required class="form-control" name="telefono" id="telefono"  placeholder="Ingrese n&uacute;mero de tel&eacute;fono del docente"/>
								</div>
               </div>
             </div>

             <div class="form-group">
               <label class="col-sm-2 control-label">Habilitar <strong style="color:red">*</strong></label>
               <div class="col-sm-5">
                 <div class="input-group">
                   <span class="input-group-addon"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                   <select name="estado" class="form-control">
                     <option value="habilitado">Si</option>
                     <option value="deshabilitado">No</option>
                   </select>
								</div>
               </div>
             </div>

           </div><!-- /.box body -->

           <div class="box-footer">
             <div class="form-group">
               <div class="col-sm-offset-2 col-sm-10">
                 <input type="submit" class="btn btn-primary btn-submit" name="Guardar" value="Guardar">
                 <a href="?module=teachers" class="btn btn-default btn-reset">Cancelar</a>
               </div>
             </div>
           </div><!-- /.box footer -->
         </form>
       </div><!-- /.box -->
     </div><!--/.col -->
   </div>   <!-- /.row -->
 </section><!-- /.content -->
<?php
}

elseif ($_GET['form']=='edit') {
 if (isset($_GET['id'])) {

     $query = mysqli_query($mysqli, "SELECT id,foto,identificacion,nombre,apellidos,email,direccion,telefono,estado FROM docentes WHERE identificacion='$_GET[id]'")
                                     or die('error: '.mysqli_error($mysqli));
     $data  = mysqli_fetch_assoc($query);
   }
?>

 <section class="content-header">
   <h1>
     <i class="fa fa-edit icon-title"></i> Editar datos Docente: <?php echo $data['nombre'].' '.$data['apellidos'];  ?>
   </h1>
   <ol class="breadcrumb">
     <li><a href="?module=start"><i class="fa fa-home"></i> Inicio </a></li>
     <li><a href="?module=teachers"> Docentes </a></li>
     <li class="active"> Modificar </li>
   </ol>
 </section>

 <!-- Main content -->
 <section class="content">
   <div class="row">
     <div class="col-md-12">
       <div class="box box-primary">
         <!-- form start -->
         <form role="form" class="form-horizontal" action="modules/teachers/process.php?act=update" method="POST" enctype="multipart/form-data">
           <div class="box-body">
             <?php
             if (isset($_GET['alert'])) {
               if($_GET['alert'] == 5){
               echo "<div class='alert alert-danger alert-dismissable'>
                       <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                       <h4>  <i class='icon fa fa-exclamation'></i> Error!</h4>
                     La imagen debe ser m&aacute;ximo de 2mb
                     </div>";
               }
             }
             if (isset($_GET['alert'])) {
               if($_GET['alert'] == 6){
               echo "<div class='alert alert-danger alert-dismissable'>
                       <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                       <h4>  <i class='icon fa fa-exclamation'></i> Error!</h4>
                     Debe escoger una imagen v&aacute;lida
                     </div>";
               }
             }
              ?>
             <div class="form-group">
               <label class="col-sm-2 control-label">Identificaci&oacute;n</label>
               <div class="col-sm-5">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-id-card" aria-hidden="true"></i></span>
                  <input type="text" class="form-control" name="id" id="id" required
                         placeholder="Cédula de extranjer&iacute;a / Cédula de ciudadan&iacute;a" value="<?php echo $data['identificacion']; ?>"
                         title="Ingrese Cédula de extranjer&iacute;a &oacute; Cédula de ciudadan&iacute;a"/>
                </div>
               </div>
             </div>

             <div class="form-group">
               <label class="col-sm-2 control-label">Foto</label>
               <div class="col-sm-5">
                 <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-upload" aria-hidden="true"></i>&nbsp;</span>
                  <input type="file" name="foto" id="foto"
                         title="Seleccione foto de docente" maxlength="2000000"/>
                </div>
               </div>
             </div>

            <div class="form-group">
              <label class="col-sm-2 control-label"><strong>Foto de perfil actual:</strong></label>
              <div class="col-sm-5">
                <img src="images/docentes/<?php echo $data['foto']; ?>" class='img-responsive' alt='User Image' style='width:180px'/>
              </div>
            </div>

             <input type="hidden" name="codigo" value="<?php echo $data['id']; ?>">
             <input type="hidden" name="estado" value="<?php echo $data['estado']; ?>">
             <input type="hidden" name="imagen" value="<?php echo $data['foto']; ?>">
             <div class="form-group">
               <label class="col-sm-2 control-label">Nombre</label>
               <div class="col-sm-5">
                 <div class="input-group">
                   <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									 <input type="text" class="form-control" name="nombre" id="nombre" required value="<?php echo $data['nombre']; ?>"
                   placeholder="Nombre del docente"/>
                </div>
               </div>
             </div>

             <div class="form-group">
               <label class="col-sm-2 control-label">Apellidos</label>
               <div class="col-sm-5">
                 <div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" value="<?php echo $data['apellidos']; ?>"class="form-control" required name="apellidos" id="apellidos"  placeholder="Apellidos del docente"/>
								</div>
               </div>
             </div>

             <div class="form-group">
               <label class="col-sm-2 control-label">E-mail</label>
               <div class="col-sm-5">
                 <div class="input-group">
                   <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
 									<input type="email" value="<?php echo $data['email']; ?>" class="form-control" required name="email" id="email"  placeholder="Ingrese correo electr&oacute;nico del docente"/>
								</div>
               </div>
             </div>

             <div class="form-group">
               <label class="col-sm-2 control-label">Direcci&oacute;n</label>
               <div class="col-sm-5">
                 <div class="input-group">
                   <span class="input-group-addon"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>
									<textarea class="form-control" required name="direccion" id="direccion"  placeholder="Ingrese direcci&oacute;n del docente"><?php echo $data['direccion']; ?></textarea>
                </div>
               </div>
             </div>

             <div class="form-group">
               <label class="col-sm-2 control-label">Tel&eacute;fono</label>
               <div class="col-sm-5">
                 <div class="input-group">
                   <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
									<input type="number" value="<?php echo $data['telefono']; ?>" required class="form-control" name="telefono" id="telefono"  placeholder="Ingrese n&uacute;mero de tel&eacute;fono del docente"/>
								</div>
               </div>
             </div>


           </div><!-- /.box body -->

           <div class="box-footer">
             <div class="form-group">
               <div class="col-sm-offset-2 col-sm-10">
                 <input type="submit" class="btn btn-primary btn-submit" name="Guardar" value="Actualizar">
                 <a href="?module=teachers" class="btn btn-default btn-reset">Cancelar</a>
               </div>
             </div>
           </div><!-- /.box footer -->
         </form>
       </div><!-- /.box -->
     </div><!--/.col -->
   </div>   <!-- /.row -->
 </section><!-- /.content -->
<?php
}
?>
