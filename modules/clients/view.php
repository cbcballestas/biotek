  <section class="content-header">
  <h1>
    <i class="fa fa-users icon-title"></i> Listado de Clientes

    <a class="btn btn-primary btn-social pull-right" href="?module=form_clients&form=add" title="agregar" data-toggle="tooltip">
      <i class="fa fa-plus"></i> Agregar
    </a>
  </h1>

</section>


<section class="content">
  <div class="row">
    <div class="col-md-12">

    <?php

    if (empty($_GET['alert'])) {
      echo "";
    }

    elseif ($_GET['alert'] == 1) {
      echo "<div class='alert alert-success alert-dismissable'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4>  <i class='icon fa fa-check-circle'></i> Exito!</h4>
             El cliente ha sido  almacenado correctamente.
            </div>";
    }

    elseif ($_GET['alert'] == 2) {
      echo "<div class='alert alert-success alert-dismissable'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4>  <i class='icon fa fa-check-circle'></i> Exito!</h4>
             Datos del Cliente modificados correcamente.
            </div>";
    }

    elseif ($_GET['alert'] == 3) {
      echo "<div class='alert alert-success alert-dismissable'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4>  <i class='icon fa fa-check-circle'></i> Exito!</h4>
            Se eliminaron los datos del Cliente
            </div>";
    }
    elseif ($_GET['alert'] == 4) {
      echo "<div class='alert alert-success alert-dismissable'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4>  <i class='icon fa fa-check-circle'></i> Exito!</h4>
            Cliente habilitado con &Eacute;xito!!
            </div>";
    }
    elseif ($_GET['alert'] == 5) {
      echo "<div class='alert alert-success alert-dismissable'>
              <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
              <h4>  <i class='icon fa fa-check-circle'></i> Exito!</h4>
            Cliente deshabilitado exitosamente!!
            </div>";
    }
    ?>

      <div class="box box-primary">
        <div class="box-body">

          <table id="dataTables1" class="table table-bordered table-striped table-hover">

            <thead>
              <tr>
                <th class="center">N°</th>
                <th class="center">Foto</th>
                <th class="center">Id</th>
                <th class="center">Nombre Completo</th>
                <th class="center">Direcci&oacute;n</th>
                <th class="center">Tel&eacute;fono</th>
                <th class="center">Procedencia</th>
                <th class="center">Estado</th>
                <th class="center">Acciones</th>
              </tr>
            </thead>
            <tbody>
            <?php
            $no = 1;
            $query = mysqli_query($mysqli, "SELECT foto,identificacion,tipo_id,nombre,apellidos,email,direccion,telefono,pais,procedencia,estado FROM clientes ORDER BY id DESC")
                                            or die('error: '.mysqli_error($mysqli));

            while ($data = mysqli_fetch_assoc($query)) {
              $act = "";
              if ($data['estado'] == "deshabilitado") {
                $act ="<tr class='danger'>";
              }else{
                $act = "<tr class='success'>";
              }
              echo $act."
                      <td width='30' class='center'>$no</td>";
                      if ($data['foto'] == "") {
                        echo "<td width='30' class='center'><img src='images/user/user-default.png' class='img-circle' alt='User Image' style='width:50px'/></td>";
                      }else{
                        echo "<td width='30' class='center'><img src='images/clientes/$data[foto]' class='img-circle' alt='User Image' style='width:50px'/></td>";
                      }
                      echo "
                      <td width='30' class='center'>$data[identificacion]</td>
                      <td width='100' class='center'>$data[nombre] $data[apellidos]</td>
                      <td width='100' align='right'>$data[direccion]</td>
                      <td width='80' align='right'>$data[telefono]</td>
                      <td width='80' class='center'>$data[procedencia],$data[pais]</td>";
                      ?>
                      <?php
                        if ($data['estado'] == "habilitado") {
                          echo "<td class='success' width='100' class='center'>$data[estado]</td>";
                        }else{
                          echo "<td class='danger' width='100' class='center'>$data[estado]</td>";
                        }

                      echo "<td class='center' width='100'>
                        <div>
                          <a data-toggle='tooltip' data-placement='top' title='modificar' style='margin-right:5px' class='btn btn-primary btn-sm' href='?module=form_clients&form=edit&id=$data[identificacion]'>
                              <i style='color:#fff' class='glyphicon glyphicon-edit'></i>
                          </a>";
                          echo "<a class='btn btn-primary btn-sm' title='Ver Informaci&oacute;n adicional'
                                            data-toggle='modal'
                                            data-target='#verMas' id='info'
                                            data-id='$data[identificacion]'>
                                        <i class='fa fa-external-link-square' aria-hidden='true'></i>
                                    </a>";
                      ?>
                          <?php
                            if ($data['estado'] == "habilitado") {
                              echo "<a data-toggle='tooltip' data-placement='top' title='Deshabilitar cliente' class='btn btn-warning btn-sm' href='modules/clients/process.php?act=disable&id=$data[identificacion]' onclick='return confirm('estas seguro de deshabilitar este cliente?');'>
                                  <i style='color:#fff' class='glyphicon glyphicon-eye-close'></i></a>";
                            }else{
                              echo "<a data-toggle='tooltip' data-placement='top' title='Habilitar cliente' class='btn btn-success btn-sm' href='modules/clients/process.php?act=enable&id=$data[identificacion]' onclick='return confirm('estas seguro de habilitar este cliente?');'>
                                  <i style='color:#fff' class='glyphicon glyphicon-eye-open'></i>
                              </a>
                              ";
                            }
                           ?>
            <?php
              echo "    </div>
                      </td>
                    </tr>";
              $no++;
            }
            ?>
            </tbody>
          </table>
          <!-- Ver más Modal start -->
                <div class="modal fade" id="verMas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">INFORMACI&Oacute;N ADICIONAL DEL
                                    CLIENTE</h4>
                            </div>
                            <div class="modal-body">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <th>Tipo_identificaci&oacute;n</th>
                                    <th>E-mail</th>
                                    </thead>
                                    <tbody id="tblDatos">

                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Ver más Modal end -->
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!--/.col -->
  </div>   <!-- /.row -->
</section><!-- /.content -->
