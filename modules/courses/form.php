<?php

if ($_GET['form']=='add') { ?>

 <section class="content-header">
   <h1>
     <i class="fa fa-graduation-cap icon-title"></i> Agregar Curso
   </h1>
   <ol class="breadcrumb">
     <li><a href="?module=start"><i class="fa fa-home"></i> Inicio </a></li>
     <li><a href="?module=courses"> Cursos </a></li>
     <li class="active"> Más </li>
   </ol>
 </section>

 <!-- Main content -->
 <section class="content">
   <div class="row">
     <div class="col-md-12">
       <div class="box box-primary">
         <!-- form start -->
         <form role="form" class="form-horizontal" action="modules/courses/process.php?act=insert" method="POST">
           <div class="box-body">
             <div class="form-group">
               <label class="col-sm-2 control-label">Nombre</label>
               <div class="col-sm-5">
                 <div class="input-group">
                   <span class="input-group-addon"><i class="fa fa-graduation-cap" aria-hidden="true"></i></span>
									 <input type="text" autofocus class="form-control" name="nombre_curso" id="nombre_curso" required placeholder="Ingrese nombre del curso"/>
                </div>
               </div>
             </div>
             <div class="form-group">
               <label class="col-sm-2 control-label">Descripci&oacute;n</label>
               <div class="col-sm-5">
                 <textarea name="descripcion_curso" required rows="8" cols="8" class="form-control"></textarea>
               </div>
             </div>

             <div class="form-group">
               <label class="col-sm-2 control-label">Valor</label>
               <div class="col-sm-5">
                 <div class="input-group">
                   <span class="input-group-addon">$.</span>
                   <input type="text" class="form-control" id="valor_curso" name="valor_curso" autocomplete="off" onKeyPress="return goodchars(event,'0123456789',this)" required>
                 </div>
               </div>
             </div>

             <div class="form-group">
               <label class="col-sm-2 control-label">Ganancia neta</label>
               <div class="col-sm-5">
                 <div class="input-group">
                   <span class="input-group-addon">$.</span>
                   <input type="text" class="form-control" id="ganancia_curso" name="ganancia_curso" autocomplete="off" onKeyPress="return goodchars(event,'0123456789',this)" required>
                 </div>
               </div>
             </div>
             <div class="form-group">
               <label class="col-sm-2 control-label">Docente</label>
               <div class="col-sm-5">
                 <?php
                 $query_obat = mysqli_query($mysqli, "SELECT id, nombre,apellidos FROM docentes ORDER BY nombre ASC")
                                                       or die('error '.mysqli_error($mysqli));
                  if ($query_obat->num_rows == 0) {
                    echo "<strong style='color:red'>Debe registrar al menos un docente para poder crear un curso</strong>";
                  }else{
                 echo "<select class='chosen-select' name='docente_curso' data-placeholder='-- Seleccione Docente --'  autocomplete='off' required>
                   <option value=''></option>";
                   while ($data_obat = mysqli_fetch_assoc($query_obat)) {
                     echo"<option value='$data_obat[id]'> $data_obat[nombre] $data_obat[apellidos] </option>";
                   }
                 echo "</select>";
               }
                   ?>
               </div>
             </div>
             <div class="form-group">
               <label class="col-sm-2 control-label">Estado</label>
               <div class="col-sm-5">
                 <div class="input-group">
                   <span class="input-group-addon"><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                   <select name="estado_curso" class="form-control">
                     <option value="activo">Activo</option>
                     <option value="inactivo">Inactivo</option>
                   </select>
								</div>
               </div>
             </div>
           </div><!-- /.box body -->

           <div class="box-footer">
             <div class="form-group">
               <div class="col-sm-offset-2 col-sm-10">
                 <input type="submit" class="btn btn-primary btn-submit" name="Guardar" value="Guardar">
                 <a href="?module=courses" class="btn btn-default btn-reset">Cancelar</a>
               </div>
             </div>
           </div><!-- /.box footer -->
         </form>
       </div><!-- /.box -->
     </div><!--/.col -->
   </div>   <!-- /.row -->
 </section><!-- /.content -->
<?php
}

elseif ($_GET['form']=='edit') {
 if (isset($_GET['id_curso'])) {

     $query = mysqli_query($mysqli, "SELECT a.id,a.nombre,a.descripcion,a.docente_id,a.valor,a.ganancia_neta,concat(b.nombre,' ',b.apellidos) 'docente',a.estado
                                     FROM cursos a
                                     INNER JOIN docentes as b ON a.docente_id=b.id
                                     WHERE a.id='$_GET[id_curso]'")
                                     or die('error: '.mysqli_error($mysqli));
     $data  = mysqli_fetch_assoc($query);
   }
?>

 <section class="content-header">
   <h1>
     <i class="fa fa-graduation-cap icon-title"></i> Modificar Curso
   </h1>
   <ol class="breadcrumb">
     <li><a href="?module=start"><i class="fa fa-home"></i> Inicio </a></li>
     <li><a href="?module=courses"> Cursos </a></li>
     <li class="active"> Modificar </li>
   </ol>
 </section>

 <!-- Main content -->
 <section class="content">
   <div class="row">
     <div class="col-md-12">
       <div class="box box-primary">
         <!-- form start -->
         <form role="form" class="form-horizontal" action="modules/courses/process.php?act=update" method="POST">
           <div class="box-body">

             <div class="form-group">
               <label class="col-sm-2 control-label">Nombre</label>
               <div class="col-sm-5">
                 <div class="input-group">
                   <span class="input-group-addon"><i class="fa fa-graduation-cap" aria-hidden="true"></i></span>
									 <input type="text" value="<?php echo $data['nombre']; ?>" class="form-control" name="nombre_curso" id="nombre_curso" required placeholder="Ingrese nombre del curso"/>
                </div>
               </div>
             </div>

             <div class="form-group">
               <label class="col-sm-2 control-label">Descripci&oacute;n</label>
               <div class="col-sm-5">
                 <textarea name="descripcion_curso" rows="10" cols="50" class="form-control" required><?php echo $data['descripcion']; ?></textarea>
               </div>
             </div>

             <div class="form-group">
               <label class="col-sm-2 control-label">Valor</label>
               <div class="col-sm-5">
                 <div class="input-group">
                   <span class="input-group-addon">$.</span>
                   <input type="text" value="<?php echo $data['valor']; ?>" class="form-control" id="valor_curso" name="valor_curso" autocomplete="off" required>
                 </div>
               </div>
             </div>

             <input type="hidden" name="estado_curso" value="<?php echo $data['estado']; ?>">
             <input type="hidden" name="id_curso" value="<?php echo $data['id']; ?>">
             <div class="form-group">
               <label class="col-sm-2 control-label">Ganancia neta</label>
               <div class="col-sm-5">
                 <div class="input-group">
                   <span class="input-group-addon">$.</span>
                   <input type="text" value="<?php echo $data['ganancia_neta']; ?>" class="form-control" id="ganancia" name="ganancia_curso" autocomplete="off" required>
                 </div>
               </div>
             </div>
             <div class="form-group">
               <label class="col-sm-2 control-label">Docente</label>
               <div class="col-sm-5">
                 <select class="chosen-select" name="docente_curso" data-placeholder="--Seleccione docente --"  autocomplete="off" required>
                   <option value="<?php echo $data['docente_id']; ?>"><?php echo $data['docente'];?></option>
                   <?php
                     $query_obat = mysqli_query($mysqli, "SELECT id, nombre,apellidos
                                                          FROM docentes
                                                          WHERE NOT id = $data[docente_id]
                                                          ORDER BY nombre ASC")
                                                           or die('error '.mysqli_error($mysqli));
                     while ($data_obat = mysqli_fetch_assoc($query_obat)) {
                       echo"<option value=\"$data_obat[id]\"> $data_obat[nombre] $data_obat[apellidos] </option>";
                     }
                   ?>
                 </select>
               </div>
             </div>
           </div><!-- /.box body -->

           <div class="box-footer">
             <div class="form-group">
               <div class="col-sm-offset-2 col-sm-10">
                 <input type="submit" class="btn btn-primary btn-submit" name="Guardar" value="Guardar">
                 <a href="?module=courses" class="btn btn-default btn-reset">Cancelar</a>
               </div>
             </div>
           </div><!-- /.box footer -->
         </form>
       </div><!-- /.box -->
     </div><!--/.col -->
   </div>   <!-- /.row -->
 </section><!-- /.content -->
<?php
}
?>
