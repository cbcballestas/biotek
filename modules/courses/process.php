<?php
session_start();


require_once "../../config/database.php";

if (empty($_SESSION['username']) && empty($_SESSION['password'])){
    echo "<meta http-equiv='refresh' content='0; url=index.php?alert=1'>";
}

else {
    if ($_GET['act']=='insert') {
        if (isset($_POST['Guardar'])) {

          $nombre=mysqli_real_escape_string($mysqli, trim($_POST['nombre_curso']));
          $descripcion=mysqli_real_escape_string($mysqli, trim($_POST['descripcion_curso']));
          $valor = mysqli_real_escape_string($mysqli, trim($_POST['valor_curso']));
          $ganancia = mysqli_real_escape_string($mysqli, trim($_POST['ganancia_curso']));
          $docente=mysqli_real_escape_string($mysqli, trim($_POST['docente_curso']));
          $estado=mysqli_real_escape_string($mysqli, trim($_POST['estado_curso']));

            $query = mysqli_query($mysqli, "INSERT INTO cursos(nombre,descripcion,valor,ganancia_neta,docente_id,estado)
                                            VALUES('$nombre', '$descripcion', '$valor', '$ganancia', '$docente', '$estado')")
                                            or die('error '.mysqli_error($mysqli));


            if ($query) {

                header("location: ../../main.php?module=courses&alert=1");
            }
        }
    }

    elseif ($_GET['act']=='update') {
        if (isset($_POST['Guardar'])) {
            if (isset($_POST['id_curso'])) {

              $id_curso=mysqli_real_escape_string($mysqli, trim($_POST['id_curso']));
              $nombre=mysqli_real_escape_string($mysqli, trim($_POST['nombre_curso']));
              $descripcion=mysqli_real_escape_string($mysqli, trim($_POST['descripcion_curso']));
              $valor = mysqli_real_escape_string($mysqli, trim($_POST['valor_curso']));
              $ganancia = mysqli_real_escape_string($mysqli, trim($_POST['ganancia_curso']));
              $docente=mysqli_real_escape_string($mysqli, trim($_POST['docente_curso']));
              $estado=mysqli_real_escape_string($mysqli, trim($_POST['estado_curso']));

                $query = mysqli_query($mysqli, "UPDATE cursos SET   nombre                = '$nombre',
                                                                    descripcion           = '$descripcion',
                                                                    valor                 = '$valor',
                                                                    ganancia_neta         = '$ganancia',
                                                                    docente_id            = '$docente',
                                                                    estado                = '$estado'
                                                              WHERE id= '$id_curso'")
                                                or die('error: '.mysqli_error($mysqli));


                if ($query) {

                    header("location: ../../main.php?module=courses&alert=2");
                }
            }
        }
    }


    elseif ($_GET['act']=='enable') {
        if (isset($_GET['id_curso'])) {
            $id_curso = $_GET['id_curso'];
            $estado = "activo";

            $query = mysqli_query($mysqli, "UPDATE cursos SET  estado = '$estado'
                                                          WHERE id = '$id_curso'")
                                            or die('error '.mysqli_error($mysqli));


            if ($query) {

                header("location: ../../main.php?module=courses&alert=3");
            }
        }
    }

    elseif ($_GET['act']=='disable') {
        if (isset($_GET['id_curso'])) {
          $id_curso = $_GET['id_curso'];
          $estado = "inactivo";

          $query = mysqli_query($mysqli, "UPDATE cursos SET  estado = '$estado'
                                                        WHERE id = '$id_curso'")
                                          or die('error '.mysqli_error($mysqli));


            if ($query) {

                header("location: ../../main.php?module=courses&alert=4");
            }
        }
    }
}
?>
