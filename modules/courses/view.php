<section class="content-header">
<h1>
  <i class="fa fa-handshake-o icon-title"></i> Asignaciones de cursos

  <a class="btn btn-primary btn-social pull-right" href="?module=form_courses&form=add" title="agregar" data-toggle="tooltip">
    <i class="fa fa-plus"></i> Agregar
  </a>
</h1>

</section>


<section class="content">
<div class="row">
  <div class="col-md-12">

  <?php

  if (empty($_GET['alert'])) {
    echo "";
  }

  elseif ($_GET['alert'] == 1) {
    echo "<div class='alert alert-success alert-dismissable'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4>  <i class='icon fa fa-check-circle'></i> Exito!</h4>
           Estudiante asignado a curso de forma &Eacute;xitosa.
          </div>";
  }

  elseif ($_GET['alert'] == 2) {
    echo "<div class='alert alert-success alert-dismissable'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4>  <i class='icon fa fa-check-circle'></i> Exito!</h4>
           Datos de la asignaci&oacute;n modificados correcamente.
          </div>";
  }

  elseif ($_GET['alert'] == 3) {
    echo "<div class='alert alert-success alert-dismissable'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4>  <i class='icon fa fa-check-circle'></i> Exito!</h4>
          Curso habilitado con &Eacute;xito!!
          </div>";
  }
  elseif ($_GET['alert'] == 4) {
    echo "<div class='alert alert-success alert-dismissable'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4>  <i class='icon fa fa-check-circle'></i> Exito!</h4>
          Curso deshabilitado exitosamente!!
          </div>";
  }
  ?>

    <div class="box box-primary">
      <div class="box-body">

        <table id="dataTables1" class="table table-bordered table-condensed table-striped table-hover">

          <thead>
            <tr>
              <th class="center">N°</th>
              <th class="center">Nombre</th>
              <th class="center">Descripci&oacute;n</th>
              <th class="center">Valor</th>
              <th class="center">Ganancia_Neta</th>
              <th class="center">Docente</th>
              <th class="center">Estado</th>
              <th class="center">Acciones</th>
            </tr>
          </thead>
          <tbody>
          <?php
          $no = 1;
          $query = mysqli_query($mysqli, "SELECT a.id,a.nombre,a.descripcion,a.valor,a.ganancia_neta,concat(b.nombre,' ',b.apellidos) 'docente',a.estado
                                FROM cursos as a INNER JOIN docentes as b ON a.docente_id=b.id
                                ORDER BY a.nombre DESC")
                                          or die('error: '.mysqli_error($mysqli));

          while ($data = mysqli_fetch_assoc($query)) {
            $act = "";
            if ($data['estado'] == "inactivo") {
              $act ="<tr class='danger'>";
            }else{
              $act = "<tr class='success'>";
            }
            echo $act."
                    <td width='30' class='center'>$no</td>
                    <td width='30' class='center'>$data[nombre]</td>
                    <td width='30' class='center'>$data[descripcion]</td>
                    <td width='80' class='center'>$$data[valor]</td>
                    <td width='100' class='center'>$$data[ganancia_neta]</td>
                    <td width='80' class='center'>$data[docente]</td>";
                    ?>
                    <?php
                      if ($data['estado'] == "activo") {
                        echo "<td class='success' width='80' class='center'>$data[estado]</td>";
                      }else{
                        echo "<td class='danger' width='80' class='center'>$data[estado]</td>";
                      }

                    echo "<td class='center' width='100'>
                      <div>
                        <a data-toggle='tooltip' data-placement='top' title='modificar' style='margin-right:5px' class='btn btn-primary btn-sm' href='?module=form_courses&form=edit&id_curso=$data[id]'>
                            <i style='color:#fff' class='glyphicon glyphicon-edit'></i>
                        </a>";
                    ?>
                        <?php
                          if ($data['estado'] == "activo") {
                            echo "<a data-toggle='tooltip' data-placement='top' title='Deshabilitar curso' class='btn btn-warning btn-sm' href='modules/courses/process.php?act=disable&id_curso=$data[id]' onclick='return confirm('estas seguro de deshabilitar este curso?');'>
                                <i style='color:#fff' class='glyphicon glyphicon-eye-close'></i></a>";
                          }else{
                            echo "<a data-toggle='tooltip' data-placement='top' title='Habilitar curso' class='btn btn-success btn-sm' href='modules/courses/process.php?act=enable&id_curso=$data[id]' onclick='return confirm('estas seguro de habilitar este curso?');'>
                                <i style='color:#fff' class='glyphicon glyphicon-eye-open'></i>
                            </a>
                            ";
                          }
                         ?>
          <?php
            echo "    </div>
                    </td>
                  </tr>";
            $no++;
          }
          ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div><!--/.col -->
</div>   <!-- /.row -->
</section><!-- /.content
