<section class="content-header">
<h1>
  <i class="fa fa-handshake-o icon-title"></i> Asignaciones de estudiantes a cursos

  <a class="btn btn-primary btn-social pull-right" href="?module=form_asignaciones&form=add" title="agregar" data-toggle="tooltip">
    <i class="fa fa-plus"></i> Agregar
  </a>
</h1>

</section>


<section class="content">
<div class="row">
  <div class="col-md-12">

  <?php

  if (empty($_GET['alert'])) {
    echo "";
  }

  elseif ($_GET['alert'] == 1) {
    echo "<div class='alert alert-success alert-dismissable'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4>  <i class='icon fa fa-check-circle'></i> Exito!</h4>
           Estudiante asignado a curso de forma &Eacute;xitosa.
          </div>";
  }

  elseif ($_GET['alert'] == 2) {
    echo "<div class='alert alert-success alert-dismissable'>
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            <h4>  <i class='icon fa fa-check-circle'></i> Exito!</h4>
           Datos de la asignaci&oacute;n modificados correcamente.
          </div>";
  }
  ?>

    <div class="box box-primary">
      <div class="box-body">

        <table id="dataTables1" class="table table-bordered table-condensed table-striped table-hover">

          <thead>
            <tr>
              <th class="center">N°</th>
              <th class="center">Estudiante</th>
              <th class="center">Curso</th>
              <th class="center">Acciones</th>
            </tr>
          </thead>
          <tbody>
          <?php
          $no = 1;
          $query = mysqli_query($mysqli, "SELECT a.id,concat(c.nombre,' ',c.apellidos) 'estudiante',b.nombre 'curso'
                                FROM cursos_estudiante as a
                                INNER JOIN cursos as b ON a.curso_id=b.id
                                INNER JOIN clientes as c ON a.estudiante_id=c.id
                                WHERE c.estado ='habilitado' AND b.estado = 'activo'
                                ORDER BY a.id DESC")
                                          or die('error: '.mysqli_error($mysqli));

          while ($data = mysqli_fetch_assoc($query)) {
            echo "  <tr>
                    <td width='30' class='center'>$no</td>
                    <td width='30' class='center'>$data[estudiante]</td>
                    <td width='80' class='center'>$data[curso]</td>
                    <td class='center' width='100'>
                      <div>
                        <a data-toggle='tooltip' data-placement='top' title='Modificar asignaci&oacute;n' style='margin-right:5px' class='btn btn-primary btn-sm' href='?module=form_asignaciones&form=edit&id=$data[id]'>
                            <i style='color:#fff' class='glyphicon glyphicon-edit'></i>
                        </a>
                    ";
                    ?>
          <?php
            echo "    </div>
                    </td>
                  </tr>";
            $no++;
          }
          ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </div><!--/.col -->
</div>   <!-- /.row -->
</section><!-- /.content
