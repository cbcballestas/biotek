<?php

if ($_GET['form']=='add') { ?>

 <section class="content-header">
   <h1>
     <i class="fa fa-handshake-o icon-title"></i> Asignar Curso
   </h1>
   <ol class="breadcrumb">
     <li><a href="?module=start"><i class="fa fa-home"></i> Inicio </a></li>
     <li><a href="?module=asignaciones"> Asignaciones </a></li>
     <li class="active"> Más </li>
   </ol>
 </section>

 <!-- Main content -->
 <section class="content">
   <div class="row">
     <div class="col-md-12">
       <div class="box box-primary">
         <!-- form start -->
         <form role="form" class="form-horizontal" action="modules/asignacion_cursos/process.php?act=insert" method="POST">
           <div class="box-body">

             <div class="form-group">
               <label class="col-sm-2 control-label">Estudiante</label>
               <div class="col-sm-5">
                 <?php
                 $query_obat = mysqli_query($mysqli, "SELECT id, nombre,apellidos FROM clientes
                                                      WHERE estado = 'habilitado'
                                                      ORDER BY nombre ASC")
                                                       or die('error '.mysqli_error($mysqli));
                  if ($query_obat->num_rows == 0) {
                    echo "<strong style='color:red'>Debe registrar al menos un estudiante para poder asignarlo a un curso</strong>";
                  }else{
                 echo "<select class='chosen-select form-control' name='estudiante' data-placeholder='-- Seleccione estudiante --'  autocomplete='off' required>
                   <option value=''></option>";
                   while ($data_obat = mysqli_fetch_assoc($query_obat)) {
                     echo"<option value='$data_obat[id]'> $data_obat[nombre] $data_obat[apellidos] </option>";
                   }
                 echo "</select>";
               }
                   ?>
               </div>
             </div>
             <div class="form-group">
               <label class="col-sm-2 control-label">Curso</label>
               <div class="col-sm-5">
                 <?php
                 $query_obat = mysqli_query($mysqli, "SELECT id, nombre FROM cursos
                                                      WHERE estado = 'activo'
                                                      ORDER BY nombre ASC")
                                                       or die('error '.mysqli_error($mysqli));
                  if ($query_obat->num_rows == 0) {
                    echo "<strong style='color:red'>Debe registrar al menos un curso para poder asignarle estudiantes</strong>";
                  }else{
                 echo "<select class='chosen-select form-control' name='curso' data-placeholder='-- Seleccione Curso a asignar --'  autocomplete='off' required>
                   <option value=''></option>";
                   while ($data_obat = mysqli_fetch_assoc($query_obat)) {
                     echo"<option value='$data_obat[id]'> $data_obat[nombre]</option>";
                   }
                 echo "</select>";
               }
                   ?>
               </div>
             </div>
           </div><!-- /.box body -->

           <div class="box-footer">
             <div class="form-group">
               <div class="col-sm-offset-2 col-sm-10">
                 <input type="submit" class="btn btn-primary btn-submit" name="Guardar" value="Asignar Curso">
                 <a href="?module=asignaciones" class="btn btn-default btn-reset">Cancelar</a>
               </div>
             </div>
           </div><!-- /.box footer -->
         </form>
       </div><!-- /.box -->
     </div><!--/.col -->
   </div>   <!-- /.row -->
 </section><!-- /.content -->
<?php
}

elseif ($_GET['form']=='edit') {
 if (isset($_GET['id'])) {

     $query = mysqli_query($mysqli, "SELECT  a.id,a.curso_id,a.estudiante_id,concat(c.nombre,' ',c.apellidos) 'estudiante',b.nombre 'curso'
                                     FROM cursos_estudiante as a
                                     INNER JOIN cursos as b ON a.curso_id=b.id
                                     INNER JOIN clientes as c ON a.estudiante_id=c.id
                                     WHERE a.id='$_GET[id]'")
                                     or die('error: '.mysqli_error($mysqli));
     $data  = mysqli_fetch_assoc($query);
   }
?>

 <section class="content-header">
   <h1>
     <i class="fa fa-graduation-cap icon-title"></i> Modificar Asignaci&oacute;n
   </h1>
   <ol class="breadcrumb">
     <li><a href="?module=start"><i class="fa fa-home"></i> Inicio </a></li>
     <li><a href="?module=asignaciones"> Asignaciones </a></li>
     <li class="active"> Modificar </li>
   </ol>
 </section>

 <!-- Main content -->
 <section class="content">
   <div class="row">
     <div class="col-md-12">
       <div class="box box-primary">
         <!-- form start -->
         <form role="form" class="form-horizontal" action="modules/asignacion_cursos/process.php?act=update" method="POST">
           <div class="box-body">
             <div class="form-group">
               <label class="col-sm-2 control-label">Estudiante:</label>
               <div class="col-sm-5">
                 <label class="control-label"><strong><?= $data['estudiante'] ;?></strong></label>
               </div>
             </div>
             <input type="hidden" name="asignacion" value="<?php echo $data['id']; ?>">
             <div class="form-group">
               <label class="col-sm-2 control-label">Curso</label>
               <div class="col-sm-5">
                 <?php
                 $query_obat = mysqli_query($mysqli, "SELECT id, nombre FROM cursos
                                                      WHERE estado = 'activo' AND
                                                      NOT id = $data[curso_id]
                                                      ORDER BY nombre ASC")
                                                       or die('error '.mysqli_error($mysqli));
                  if ($query_obat->num_rows == 0) {
                    echo "<strong style='color:red'>Debe registrar al menos un curso para poder asignarle estudiantes</strong>";
                  }else{
                 echo "<select class='chosen-select form-control' name='curso' data-placeholder='--Seleccione curso --'  autocomplete='off' required>
                   <option value='$data[curso_id]'>$data[curso]</option>";
                   while ($data_obat = mysqli_fetch_assoc($query_obat)) {
                     echo"<option value='$data_obat[id]'> $data_obat[nombre]</option>";
                   }
                 echo "</select>";
               }
                   ?>
               </div>
             </div>

           </div><!-- /.box body -->

           <div class="box-footer">
             <div class="form-group">
               <div class="col-sm-offset-2 col-sm-10">
                 <input type="submit" class="btn btn-primary btn-submit" name="Guardar" value="Guardar">
                 <a href="?module=asignaciones" class="btn btn-default btn-reset">Cancelar</a>
               </div>
             </div>
           </div><!-- /.box footer -->
         </form>
       </div><!-- /.box -->
     </div><!--/.col -->
   </div>   <!-- /.row -->
 </section><!-- /.content -->
<?php
}
?>
