-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.14 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.4.0.5174
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla medisys.clientes
CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `foto` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `identificacion` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_id` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `pais` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `procedencia` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `estado` enum('habilitado','deshabilitado') COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla medisys.clientes: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` (`id`, `foto`, `identificacion`, `tipo_id`, `nombre`, `apellidos`, `email`, `direccion`, `telefono`, `pais`, `procedencia`, `estado`) VALUES
	(5, '', '454545', 'cedula', 'Alejandra Maria', 'Lopez', 'ale@mail.com', 'comuna 10', '88990000', 'Colombia', 'Tumaco', 'habilitado'),
	(6, '', '6058658', 'extranjeria', 'Rodrigo Manuel', 'Lara LÃ³pez', 'rodrigo@mail.com', 'Barrio la esperanza', '56226968733', 'Chile', 'Santiago', 'habilitado'),
	(7, '1504548900_LOGO-BIOTEK-PMU_png.png', '1111111', 'cedula', 'Luz Estela', 'Franco MicropigmentaciÃ³n', 'biotek@luzestalefranco.com', 'Centro comercial Ronda Real', '555555555555', 'Colombia', 'Cartagena', 'habilitado'),
	(8, '1504548868_maxresdefault.jpg', '1234556', 'cedula', 'Pedro Manuel', 'Rada RodrÃ­guez', 'pradar@gmail.com', 'Barrio los Corales', '6543213', 'Venezuela', 'Caracas', 'habilitado'),
	(9, '1504552462_WIN_20161024_171350.JPG', '44444', 'cedula', 'Lucas', 'Vasquez', 'lucas@gmail.com', 'Barrio el rempujo sector la plaza', '3124567888', 'Colombia', 'Barranquilla', 'habilitado');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;

-- Volcando estructura para tabla medisys.cursos
CREATE TABLE IF NOT EXISTS `cursos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `valor` double(10,0) NOT NULL,
  `ganancia_neta` double(10,0) NOT NULL,
  `docente_id` int(10) NOT NULL,
  `estado` enum('activo','inactivo') COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cursos_docentes` (`docente_id`),
  CONSTRAINT `FK_cursos_docentes` FOREIGN KEY (`docente_id`) REFERENCES `docentes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla medisys.cursos: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `cursos` DISABLE KEYS */;
INSERT INTO `cursos` (`id`, `nombre`, `descripcion`, `valor`, `ganancia_neta`, `docente_id`, `estado`) VALUES
	(1, 'Taller bÃ¡sico Y ESPECIALIZADO de micropigmentaciÃ³n', 'Transmitir los conocimientos necesarios para aplicar de manera eficaz y acorde a la estÃ©tica de cada rostro la tÃ©cnica de la MicropigmentaciÃ³n en cejas, pÃ¡rpados y labios, con el fin de corregir, modificar, embellecer y equilibrar de forma semipermanente determinados rasgos.', 3000000, 1500000, 4, 'activo'),
	(2, 'CURSO DE PRUEBA', 'Este es un curso para testing', 2000000, 1000000, 5, 'activo'),
	(3, 'Cursos Micropigmentacion, Microblading,cejas Pelo , PestaÃ±as', 'Descripcion de prueba', 5000000, 3000000, 5, 'inactivo');
/*!40000 ALTER TABLE `cursos` ENABLE KEYS */;

-- Volcando estructura para tabla medisys.cursos_estudiante
CREATE TABLE IF NOT EXISTS `cursos_estudiante` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `estudiante_id` int(10) NOT NULL,
  `curso_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cursos_estudiante_clientes` (`estudiante_id`),
  KEY `FK_cursos_estudiante_cursos` (`curso_id`),
  CONSTRAINT `FK_cursos_estudiante_clientes` FOREIGN KEY (`estudiante_id`) REFERENCES `clientes` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_cursos_estudiante_cursos` FOREIGN KEY (`curso_id`) REFERENCES `cursos` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla medisys.cursos_estudiante: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `cursos_estudiante` DISABLE KEYS */;
INSERT INTO `cursos_estudiante` (`id`, `estudiante_id`, `curso_id`) VALUES
	(1, 5, 1),
	(2, 8, 2);
/*!40000 ALTER TABLE `cursos_estudiante` ENABLE KEYS */;

-- Volcando estructura para tabla medisys.docentes
CREATE TABLE IF NOT EXISTS `docentes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `foto` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `identificacion` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(35) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `estado` enum('habilitado','deshabilitado') COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- Volcando datos para la tabla medisys.docentes: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `docentes` DISABLE KEYS */;
INSERT INTO `docentes` (`id`, `foto`, `identificacion`, `nombre`, `apellidos`, `email`, `direccion`, `telefono`, `estado`) VALUES
	(4, '1504539178_login.jpg', '12345', 'Luis Manuel', 'Torres PeÃ±aranda', 'luis@mail.com', 'Barrio los alpes Mz D L 25', '6678965', 'habilitado'),
	(5, '1504552238_maxresdefault.jpg', '333333', 'Juan Manuel', 'PÃ©rez LÃ³pez', 'juan@mail.com', 'Barrio la marÃ­a', '6653245', 'habilitado');
/*!40000 ALTER TABLE `docentes` ENABLE KEYS */;

-- Volcando estructura para tabla medisys.medicamentos
CREATE TABLE IF NOT EXISTS `medicamentos` (
  `codigo` varchar(7) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `precio_compra` int(11) NOT NULL,
  `precio_venta` int(11) NOT NULL,
  `unidad` varchar(20) NOT NULL,
  `stock` int(11) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_user` int(3) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`codigo`),
  KEY `created_user` (`created_user`),
  KEY `updated_user` (`updated_user`),
  CONSTRAINT `medicamentos_ibfk_1` FOREIGN KEY (`created_user`) REFERENCES `usuarios` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `medicamentos_ibfk_2` FOREIGN KEY (`updated_user`) REFERENCES `usuarios` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla medisys.medicamentos: ~73 rows (aproximadamente)
/*!40000 ALTER TABLE `medicamentos` DISABLE KEYS */;
INSERT INTO `medicamentos` (`codigo`, `nombre`, `precio_compra`, `precio_venta`, `unidad`, `stock`, `created_user`, `created_date`, `updated_user`, `updated_date`) VALUES
	('B000001', 'EA427A-Biolip 27', 148790, 380000, 'spray', 1, 3, '2017-08-17 02:09:18', 1, '2017-08-26 14:55:58'),
	('B000002', 'EA482A-Biolip 35', 148790, 380000, 'spray', 1, 3, '2017-08-17 02:12:20', 1, '2017-08-26 14:53:25'),
	('B000003', 'EA507A-Biolip 42', 148790, 380000, 'spray', 0, 3, '2017-08-17 02:18:19', 1, '2017-08-26 14:54:21'),
	('B000004', 'EA432A-Yellow', 0, 0, 'tubo', 1, 3, '2017-08-17 02:19:37', 3, '2017-08-26 14:57:26'),
	('B000005', 'EA433A-Orange', 0, 0, 'tubo', 2, 3, '2017-08-17 02:22:14', 3, '2017-08-26 14:58:24'),
	('B000006', 'EA434A-Mineral Red', 0, 0, 'tubo', 1, 3, '2017-08-17 02:23:21', 3, '2017-08-26 14:59:00'),
	('B000007', 'EA435A-Bright Red', 0, 0, 'tubo', 1, 3, '2017-08-17 02:24:08', 3, '2017-08-26 15:01:17'),
	('B000008', 'EA436A-Black', 0, 0, 'tubo', 0, 3, '2017-08-17 02:24:49', 3, '2017-08-17 02:24:49'),
	('B000009', 'EA443A-Blond 7', 0, 0, 'tubo', 0, 3, '2017-08-17 02:25:37', 3, '2017-08-17 02:25:37'),
	('B000010', 'EA500A-Blond 9', 0, 0, 'tubo', 0, 3, '2017-08-17 02:26:38', 3, '2017-08-17 02:26:38'),
	('B000011', 'EA445A-Brown 1', 0, 0, 'tubo', 0, 3, '2017-08-17 02:27:15', 3, '2017-08-17 02:27:15'),
	('B000012', 'EA446A-Brown 2', 0, 380000, 'tubo', 1, 3, '2017-08-17 02:28:31', 3, '2017-08-26 15:03:46'),
	('B000013', 'EA447A-Brown 3', 0, 380000, 'tubo', 1, 3, '2017-08-17 02:29:41', 3, '2017-08-26 15:03:59'),
	('B000014', 'EA448A-Brown 4', 0, 380000, 'tubo', 0, 3, '2017-08-17 02:32:43', 3, '2017-08-17 02:32:43'),
	('B000015', 'EA449A-Brown 5', 0, 380000, 'tubo', 1, 3, '2017-08-17 02:36:09', 3, '2017-08-26 15:04:27'),
	('B000016', 'EA450A-Brown 6', 0, 380000, 'tubo', 1, 3, '2017-08-17 02:36:56', 3, '2017-08-26 15:04:36'),
	('B000017', 'EA491A-Brown 7', 0, 380000, 'tubo', 3, 3, '2017-08-17 02:37:54', 3, '2017-08-26 15:04:44'),
	('B000018', 'EA503A-Brown 8', 0, 380000, 'tubo', 4, 3, '2017-08-17 02:39:24', 3, '2017-08-26 15:04:53'),
	('B000019', 'EA452A-Brun 2', 0, 380000, 'tubo', 3, 3, '2017-08-17 02:42:08', 3, '2017-08-26 15:05:03'),
	('B000020', 'EA453A-Brun 3', 0, 380000, 'tubo', 3, 3, '2017-08-17 02:43:02', 3, '2017-08-26 15:05:11'),
	('B000021', 'EA454A-Brun 4', 0, 380000, 'tubo', 0, 3, '2017-08-17 02:45:14', 3, '2017-08-17 02:45:14'),
	('B000022', 'EA502A-Brun 5', 0, 380000, 'tubo', 1, 3, '2017-08-17 02:52:25', 3, '2017-08-26 15:05:27'),
	('B000023', 'EA501A-Blond 10', 0, 380000, 'tubo', 1, 3, '2017-08-17 03:00:23', 3, '2017-08-26 15:05:35'),
	('B000024', 'EA455A-Warm Grey', 0, 380000, 'tubo', 1, 3, '2017-08-17 03:01:22', 3, '2017-08-26 15:05:54'),
	('B000025', 'EA429A-Corrector 1 (orange)', 0, 380000, 'tubo', 4, 3, '2017-08-17 03:02:41', 3, '2017-08-26 15:06:10'),
	('B000026', 'EA430A-Corrector 2 (Green)', 0, 380000, 'tubo', 6, 3, '2017-08-17 03:03:58', 3, '2017-08-26 15:06:20'),
	('B000027', 'EA456A-Black Liner .', 0, 380000, 'tubo', 3, 3, '2017-08-17 03:05:02', 3, '2017-08-26 15:06:30'),
	('B000028', 'EA458A-Strong Black', 0, 380000, 'tubo', 4, 3, '2017-08-17 03:06:53', 3, '2017-08-26 15:06:42'),
	('B000029', 'EA494A-Green Liner', 0, 380000, 'tubo', 2, 3, '2017-08-17 03:08:00', 3, '2017-08-26 15:06:51'),
	('B000030', 'EA509A-Violet Liner', 0, 380000, 'tubo', 1, 3, '2017-08-17 03:08:46', 3, '2017-08-26 15:06:59'),
	('B000031', 'EA471A-Skin 13', 0, 380000, 'tubo', 0, 3, '2017-08-17 03:14:37', 3, '2017-08-17 03:14:37'),
	('B000032', 'EA474A-Skin 16', 0, 380000, 'tubo', 0, 3, '2017-08-17 03:20:19', 3, '2017-08-17 03:20:19'),
	('B000033', 'EA486A-Skin 20', 0, 380000, 'tubo', 1, 3, '2017-08-17 03:21:00', 3, '2017-08-26 15:07:18'),
	('B000034', 'EA489A-Skin 23', 0, 380000, 'tubo', 1, 3, '2017-08-17 03:21:45', 3, '2017-08-26 15:07:25'),
	('B000035', 'EA516A-Skin 30', 0, 380000, 'tubo', 1, 3, '2017-08-17 03:22:24', 3, '2017-08-26 15:07:32'),
	('B000036', 'EA522A-Kiss Kiss', 0, 380000, 'tubo', 1, 3, '2017-08-17 03:24:58', 3, '2017-08-26 15:07:39'),
	('B000037', 'EA525A Extreme', 0, 380000, 'tubo', 0, 3, '2017-08-17 03:26:32', 3, '2017-08-17 03:26:32'),
	('B000038', 'EA527A-Marilyn', 0, 380000, 'tubo', 2, 3, '2017-08-17 03:29:00', 3, '2017-08-26 15:08:14'),
	('B000039', 'EA532A-Arabica .  (Intense)', 0, 380000, 'tubo', 1, 3, '2017-08-17 03:30:01', 3, '2017-08-26 15:08:44'),
	('B000040', 'EA533A-Terre .  (Intense)', 0, 380000, 'tubo', 1, 3, '2017-08-17 03:30:54', 3, '2017-08-26 15:08:58'),
	('B000041', 'EA534A-Ocra    (Intense)', 0, 380000, 'tubo', 4, 3, '2017-08-17 03:31:35', 3, '2017-08-26 15:09:09'),
	('B000042', 'EA535A-Brownie . (Intense)', 0, 380000, 'tubo', 3, 3, '2017-08-17 03:32:34', 3, '2017-08-26 15:11:12'),
	('B000043', 'EA536A-Bronze (Intense)', 0, 380000, 'tubo', 1, 3, '2017-08-17 03:33:44', 3, '2017-08-26 15:11:22'),
	('B000044', 'EA537A-Camel  (Intense)', 0, 380000, 'tubo', 0, 3, '2017-08-17 03:34:43', 3, '2017-08-17 03:34:43'),
	('B000045', 'EA333A-Pure Red .  (Intense)', 0, 380000, 'tubo', 1, 3, '2017-08-17 03:35:32', 3, '2017-08-26 15:11:37'),
	('B000046', 'EA335A-Antique Rose . (Intense)', 0, 380000, 'tubo', 2, 3, '2017-08-17 03:36:20', 3, '2017-08-26 15:11:47'),
	('B000047', 'EA521A-Sunset . (Intense)', 0, 380000, 'tubo', 0, 3, '2017-08-17 03:37:15', 3, '2017-08-17 03:37:15'),
	('B000048', 'EA528A-Rouge Intense Hot Pink   ( Intense)', 0, 380000, 'tubo', 2, 3, '2017-08-17 03:38:25', 3, '2017-08-26 15:12:32'),
	('B000049', 'EA524A- Glam .  ( Intense)', 0, 380000, 'tubo', 1, 3, '2017-08-17 03:39:06', 3, '2017-08-26 15:13:11'),
	('B000050', 'EA523A-Orange Red .   ( Intense)', 0, 380000, 'tubo', 1, 3, '2017-08-17 03:40:29', 3, '2017-08-26 15:13:20'),
	('B000051', 'EM325A-Trico Corrector   ( Co Colors)', 0, 105000, 'tubo', 0, 3, '2017-08-17 04:04:35', 3, '2017-08-17 04:04:35'),
	('B000052', 'EM327A-Trico Brown .   (Co Colors)', 0, 105000, 'tubo', 0, 3, '2017-08-17 04:05:56', 3, '2017-08-17 04:05:56'),
	('B000053', 'EM328A-Trico Brun .  (Co Colors)', 0, 105000, 'tubo', 0, 3, '2017-08-17 04:06:54', 3, '2017-08-17 04:06:54'),
	('B000054', 'E246A-Bio Clor Diluente Clinik', 0, 250000, 'tubo', 6, 3, '2017-08-17 04:27:35', 3, '2017-08-26 15:14:15'),
	('B000055', 'Blades Microblading', 0, 0, 'caja', 2, 3, '2017-08-17 04:35:05', 3, '2017-08-26 15:15:02'),
	('B000056', 'Anestesia piel Cerrada', 0, 60000, 'tubo', 0, 3, '2017-08-17 04:36:01', 3, '2017-08-17 04:36:01'),
	('B000057', 'Anestesia piel abierta', 0, 0, 'tubo', 0, 3, '2017-08-17 04:36:24', 3, '2017-08-17 04:36:24'),
	('B000058', 'Giant Sun', 0, 250000, 'tubo', 2, 3, '2017-08-17 04:36:54', 3, '2017-08-26 15:15:47'),
	('B000059', 'E501.5A- Cartridge 1 point .  (World Class-Stilus)', 0, 0, 'pieza', 10, 3, '2017-08-18 04:23:02', 1, '2017-08-26 15:30:27'),
	('B000060', 'E500.5A-Cartridge 1 point Romb . ( Word Class-Stil', 0, 0, 'pieza', 35, 3, '2017-08-18 04:29:23', 1, '2017-08-26 15:37:29'),
	('B000061', 'E509.5A-Cartuccia 1 punta color ( World-Stilus)', 0, 0, 'pieza', 15, 3, '2017-08-18 04:30:55', 1, '2017-08-26 15:29:23'),
	('B000062', 'E519.5A- Cartridge 1 Point HD  ( World-Stilus)', 0, 0, 'pieza', 15, 3, '2017-08-18 04:32:35', 1, '2017-08-26 15:19:18'),
	('B000063', 'E502.5A-Cartridge 3 points  ( World Class-Stilus)', 0, 0, 'pieza', 5, 3, '2017-08-18 04:34:06', 1, '2017-08-26 15:38:01'),
	('B000064', 'E515.5A- Cartridge 3 point soft liner ( World Clas', 0, 0, 'pieza', 16, 3, '2017-08-18 04:36:53', 1, '2017-08-26 15:33:03'),
	('B000065', 'E504.5A- Cartridge 5 points shader  ( World Class-', 0, 0, 'pieza', 20, 3, '2017-08-18 04:38:17', 1, '2017-08-26 15:26:51'),
	('B000066', 'E507.5A-Cartridge 12 points . ( World Class-Stilus', 0, 0, 'pieza', 4, 3, '2017-08-19 03:10:17', 1, '2017-08-26 15:38:19'),
	('B000067', 'E516.5A-Cartridge 6 points stope  (World Class-Sti', 0, 0, 'pieza', 10, 3, '2017-08-19 03:12:40', 1, '2017-08-26 15:37:45'),
	('B000068', '513.5A-Cartridge 5 points cross ( World Class)', 0, 0, 'pieza', 11, 3, '2017-08-19 03:27:48', 1, '2017-08-26 15:39:36'),
	('B000069', 'E518.5A-Cartridge 9 points magnum ( World Class-st', 0, 0, 'pieza', 4, 3, '2017-08-19 03:31:57', 1, '2017-08-26 15:35:48'),
	('B000070', 'EA436A - Black', 148790, 380000, 'spray', 0, 1, '2017-08-26 15:02:30', 1, '2017-08-26 15:02:30'),
	('B000071', 'Anestesia Piel Cerrada', 0, 0, 'tubo', 0, 1, '2017-08-26 15:17:57', 1, '2017-08-26 15:17:57'),
	('B000072', 'Anestesia Piel Abierta', 0, 0, 'frasco', 0, 1, '2017-08-26 15:18:15', 1, '2017-08-26 15:18:15'),
	('B000073', '0.25X40 Agujas Acupuntura', 210, 3000, 'pieza', 420, 1, '2017-08-26 15:42:28', 1, '2017-08-26 15:43:39');
/*!40000 ALTER TABLE `medicamentos` ENABLE KEYS */;

-- Volcando estructura para tabla medisys.transaccion_medicamentos
CREATE TABLE IF NOT EXISTS `transaccion_medicamentos` (
  `codigo_transaccion` varchar(15) NOT NULL,
  `fecha` date NOT NULL,
  `codigo` varchar(7) NOT NULL,
  `numero` int(11) NOT NULL,
  `created_user` int(3) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tipo_transaccion` varchar(50) NOT NULL,
  `cliente_id` varchar(20) NOT NULL,
  PRIMARY KEY (`codigo_transaccion`),
  KEY `id_barang` (`codigo`),
  KEY `created_user` (`created_user`),
  CONSTRAINT `transaccion_medicamentos_ibfk_1` FOREIGN KEY (`codigo`) REFERENCES `medicamentos` (`codigo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `transaccion_medicamentos_ibfk_2` FOREIGN KEY (`created_user`) REFERENCES `usuarios` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla medisys.transaccion_medicamentos: ~53 rows (aproximadamente)
/*!40000 ALTER TABLE `transaccion_medicamentos` DISABLE KEYS */;
INSERT INTO `transaccion_medicamentos` (`codigo_transaccion`, `fecha`, `codigo`, `numero`, `created_user`, `created_date`, `tipo_transaccion`, `cliente_id`) VALUES
	('TM-2017-0000001', '2017-08-26', 'B000001', 1, 1, '2017-08-26 14:55:31', 'Entrada', '1111111'),
	('TM-2017-0000002', '2017-08-26', 'B000004', 1, 1, '2017-08-26 14:57:26', 'Entrada', '1111111'),
	('TM-2017-0000003', '2017-08-26', 'B000005', 1, 1, '2017-08-26 14:57:52', 'Entrada', '1111111'),
	('TM-2017-0000004', '2017-08-26', 'B000005', 1, 1, '2017-08-26 14:58:24', 'Entrada', '1111111'),
	('TM-2017-0000005', '2017-08-26', 'B000006', 1, 1, '2017-08-26 14:59:00', 'Entrada', '1111111'),
	('TM-2017-0000006', '2017-08-26', 'B000007', 1, 1, '2017-08-26 15:01:17', 'Entrada', '1111111'),
	('TM-2017-0000007', '2017-08-26', 'B000012', 1, 1, '2017-08-26 15:03:46', 'Entrada', '1111111'),
	('TM-2017-0000008', '2017-08-26', 'B000013', 1, 1, '2017-08-26 15:03:59', 'Entrada', '1111111'),
	('TM-2017-0000009', '2017-08-26', 'B000015', 1, 1, '2017-08-26 15:04:27', 'Entrada', '1111111'),
	('TM-2017-0000010', '2017-08-26', 'B000016', 1, 1, '2017-08-26 15:04:36', 'Entrada', '1111111'),
	('TM-2017-0000011', '2017-08-26', 'B000017', 3, 1, '2017-08-26 15:04:44', 'Entrada', '1111111'),
	('TM-2017-0000012', '2017-08-26', 'B000018', 4, 1, '2017-08-26 15:04:53', 'Entrada', '1111111'),
	('TM-2017-0000013', '2017-08-26', 'B000019', 3, 1, '2017-08-26 15:05:03', 'Entrada', '1111111'),
	('TM-2017-0000014', '2017-08-26', 'B000020', 3, 1, '2017-08-26 15:05:11', 'Entrada', '1111111'),
	('TM-2017-0000015', '2017-08-26', 'B000022', 1, 1, '2017-08-26 15:05:27', 'Entrada', '1111111'),
	('TM-2017-0000016', '2017-08-26', 'B000023', 1, 1, '2017-08-26 15:05:35', 'Entrada', '1111111'),
	('TM-2017-0000017', '2017-08-26', 'B000024', 1, 1, '2017-08-26 15:05:54', 'Entrada', '1111111'),
	('TM-2017-0000018', '2017-08-26', 'B000025', 4, 1, '2017-08-26 15:06:10', 'Entrada', '1111111'),
	('TM-2017-0000019', '2017-08-26', 'B000026', 6, 1, '2017-08-26 15:06:20', 'Entrada', '1111111'),
	('TM-2017-0000020', '2017-08-26', 'B000027', 3, 1, '2017-08-26 15:06:30', 'Entrada', '1111111'),
	('TM-2017-0000021', '2017-08-26', 'B000028', 4, 1, '2017-08-26 15:06:42', 'Entrada', '1111111'),
	('TM-2017-0000022', '2017-08-26', 'B000029', 2, 1, '2017-08-26 15:06:51', 'Entrada', '1111111'),
	('TM-2017-0000023', '2017-08-26', 'B000030', 1, 1, '2017-08-26 15:06:59', 'Entrada', '1111111'),
	('TM-2017-0000024', '2017-08-26', 'B000033', 1, 1, '2017-08-26 15:07:18', 'Entrada', '1111111'),
	('TM-2017-0000025', '2017-08-26', 'B000034', 1, 1, '2017-08-26 15:07:25', 'Entrada', '1111111'),
	('TM-2017-0000026', '2017-08-26', 'B000035', 1, 1, '2017-08-26 15:07:32', 'Entrada', '1111111'),
	('TM-2017-0000027', '2017-08-26', 'B000036', 1, 1, '2017-08-26 15:07:39', 'Entrada', '1111111'),
	('TM-2017-0000028', '2017-08-26', 'B000038', 2, 1, '2017-08-26 15:08:14', 'Entrada', '1111111'),
	('TM-2017-0000029', '2017-08-26', 'B000039', 1, 1, '2017-08-26 15:08:44', 'Entrada', '1111111'),
	('TM-2017-0000030', '2017-08-26', 'B000040', 1, 1, '2017-08-26 15:08:58', 'Entrada', '1111111'),
	('TM-2017-0000031', '2017-08-26', 'B000041', 4, 1, '2017-08-26 15:09:09', 'Entrada', '1111111'),
	('TM-2017-0000032', '2017-08-26', 'B000042', 3, 1, '2017-08-26 15:11:12', 'Entrada', '1111111'),
	('TM-2017-0000033', '2017-08-26', 'B000043', 1, 1, '2017-08-26 15:11:22', 'Entrada', '1111111'),
	('TM-2017-0000034', '2017-08-26', 'B000045', 1, 1, '2017-08-26 15:11:37', 'Entrada', '1111111'),
	('TM-2017-0000035', '2017-08-26', 'B000046', 2, 1, '2017-08-26 15:11:47', 'Entrada', '1111111'),
	('TM-2017-0000036', '2017-08-26', 'B000048', 2, 1, '2017-08-26 15:12:32', 'Entrada', '1111111'),
	('TM-2017-0000037', '2017-08-26', 'B000049', 1, 1, '2017-08-26 15:13:11', 'Entrada', '1111111'),
	('TM-2017-0000038', '2017-08-26', 'B000050', 1, 1, '2017-08-26 15:13:20', 'Entrada', '1111111'),
	('TM-2017-0000039', '2017-08-26', 'B000054', 6, 1, '2017-08-26 15:14:15', 'Entrada', '1111111'),
	('TM-2017-0000040', '2017-08-26', 'B000055', 2, 1, '2017-08-26 15:15:02', 'Entrada', '1111111'),
	('TM-2017-0000041', '2017-08-26', 'B000058', 2, 1, '2017-08-26 15:15:47', 'Entrada', '1111111'),
	('TM-2017-0000042', '2017-08-26', 'B000062', 15, 1, '2017-08-26 15:18:56', 'Entrada', '1111111'),
	('TM-2017-0000043', '2017-08-26', 'B000060', 35, 1, '2017-08-26 15:23:58', 'Entrada', '1111111'),
	('TM-2017-0000044', '2017-08-26', 'B000065', 20, 1, '2017-08-26 15:26:51', 'Entrada', '1111111'),
	('TM-2017-0000045', '2017-08-26', 'B000061', 15, 1, '2017-08-26 15:29:23', 'Entrada', '1111111'),
	('TM-2017-0000046', '2017-08-26', 'B000059', 10, 1, '2017-08-26 15:30:15', 'Entrada', '1111111'),
	('TM-2017-0000047', '2017-08-26', 'B000064', 16, 1, '2017-08-26 15:32:33', 'Entrada', '1111111'),
	('TM-2017-0000048', '2017-08-26', 'B000068', 11, 1, '2017-08-26 15:34:03', 'Entrada', '1111111'),
	('TM-2017-0000049', '2017-08-26', 'B000069', 4, 1, '2017-08-26 15:35:48', 'Entrada', '1111111'),
	('TM-2017-0000050', '2017-08-26', 'B000067', 10, 1, '2017-08-26 15:36:19', 'Entrada', '1111111'),
	('TM-2017-0000051', '2017-08-26', 'B000063', 5, 1, '2017-08-26 15:36:49', 'Entrada', '1111111'),
	('TM-2017-0000052', '2017-08-26', 'B000066', 4, 1, '2017-08-26 15:37:12', 'Entrada', '1111111'),
	('TM-2017-0000053', '2017-08-26', 'B000073', 420, 1, '2017-08-26 15:42:44', 'Entrada', '1111111');
/*!40000 ALTER TABLE `transaccion_medicamentos` ENABLE KEYS */;

-- Volcando estructura para tabla medisys.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_user` int(3) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `name_user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telefono` varchar(13) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `permisos_acceso` enum('Super Admin','Gerente','Almacen') NOT NULL,
  `status` enum('activo','bloqueado') NOT NULL DEFAULT 'activo',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_user`),
  KEY `level` (`permisos_acceso`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla medisys.usuarios: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id_user`, `username`, `name_user`, `password`, `email`, `telefono`, `foto`, `permisos_acceso`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'Sistemas Webs', '21232f297a57a5a743894a0e4a801fc3', 'info@sist.com', '7025', 'user-default.png', 'Super Admin', 'activo', '2017-04-01 03:15:15', '2017-07-25 18:35:23'),
	(2, 'juan', 'juan', 'a94652aa97c7211ba8954dd15a3cf838', 'juab@juan.com', '12000', NULL, 'Almacen', 'activo', '2017-07-25 17:34:03', '2017-07-25 17:42:00'),
	(3, 'samuel', 'samuel', 'd8ae5776067290c4712fa454006c8ec6', '', '', NULL, 'Almacen', 'activo', '2017-08-21 09:21:42', '2017-08-21 09:37:47');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
