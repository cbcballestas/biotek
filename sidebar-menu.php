<?php

if ($_SESSION['permisos_acceso']=='Super Admin') { ?>

    <ul class="sidebar-menu">
        <li class="header">MENU</li>

	<?php

	if ($_GET["module"]=="start") {
		$active_home="active";
	} else {
		$active_home="";
	}
	?>
		<li class="<?php echo $active_home;?>">
			<a href="?module=start" style="color:#E4AEE8"><i class="fa fa-home"></i> Inicio </a>
	  	</li>
	<?php

  if ($_GET["module"]=="medicines" || $_GET["module"]=="form_medicines") { ?>
    <li class="active">
      <a href="?module=medicines" style="color:#E4AEE8"><i class="fa fa-folder"></i> Datos de suminisitros </a>
      </li>
  <?php
  }

  else { ?>
    <li>
      <a href="?module=medicines" style="color:#E4AEE8"><i class="fa fa-folder"></i> Datos de suministros </a>
      </li>
  <?php
  }


  if ($_GET["module"]=="medicines_transaction" || $_GET["module"]=="form_medicines_transaction") { ?>
    <li class="active">
      <a href="?module=medicines_transaction" style="color:#E4AEE8"><i class="fa fa-clone"></i> Registro de suministros </a>
      </li>
  <?php
  }

  else { ?>
    <li>
      <a href="?module=medicines_transaction" style="color:#E4AEE8"><i class="fa fa-clone"></i> Registro de suministros </a>
      </li>
  <?php
  }

	if ($_GET["module"]=="stock_inventory") { ?>
		<li class="active treeview">
          	<a href="javascript:void(0);" style="color:#E4AEE8">
            	<i class="fa fa-file-text"></i> <span>Reportes</span> <i class="fa fa-angle-left pull-right"></i>
          	</a>
      		<ul class="treeview-menu">
        		<li class="active"><a href="?module=stock_inventory" style="color:#E4AEE8"><i class="fa fa-circle-o"></i> Stock de Suministros </a></li>
        		<li><a href="?module=stock_report" style="color:#E4AEE8"><i class="fa fa-circle-o"></i> Registro de suministros</a></li>
      		</ul>
    	</li>
    <?php
	}

	elseif ($_GET["module"]=="stock_report") { ?>
		<li class="active treeview">
          	<a href="javascript:void(0);" style="color:#E4AEE8">
            	<i class="fa fa-file-text"></i> <span>Reportes</span> <i class="fa fa-angle-left pull-right"></i>
          	</a>
      		<ul class="treeview-menu">
        		<li><a href="?module=stock_inventory" style="color:#E4AEE8"><i class="fa fa-circle-o"></i> Stock de Suministros </a></li>
        		<li class="active"><a href="?module=stock_report" style="color:#E4AEE8"><i class="fa fa-circle-o"></i> Registro de suministros </a></li>
      		</ul>
    	</li>
    <?php
	}

	else { ?>
		<li class="treeview">
          	<a href="javascript:void(0);" style="color:#E4AEE8">
            	<i class="fa fa-file-text"></i> <span>Reportes</span> <i class="fa fa-angle-left pull-right"></i>
          	</a>
      		<ul class="treeview-menu">
        		<li><a href="?module=stock_inventory" style="color:#E4AEE8"><i class="fa fa-circle-o"></i> Stock de Suministros </a></li>
        		<li><a href="?module=stock_report" style="color:#E4AEE8"><i class="fa fa-circle-o"></i> Registro de Suministros </a></li>
      		</ul>
    	</li>
    <?php
	}


	if ($_GET["module"]=="user" || $_GET["module"]=="form_user") { ?>
		<li class="active">
			<a href="?module=user" style="color:#E4AEE8"><i class="fa fa-user"></i> Administrar usuarios</a>
	  	</li>
	<?php
	}

	else { ?>
		<li>
			<a href="?module=user" style="color:#E4AEE8"><i class="fa fa-user"></i> Administrar usuarios</a>
	  	</li>
	<?php
	}

  if ($_GET["module"]=="clients" || $_GET["module"]=="form_clients") { ?>
    <li class="active">
      <a href="?module=clients" style="color:#E4AEE8"><i class="fa fa-users"></i>Clientes </a>
      </li>
  <?php
  }

  else { ?>
    <li>
      <a href="?module=clients" style="color:#E4AEE8"><i class="fa fa-users"></i> Clientes </a>
      </li>
  <?php
  }

  if ($_GET["module"]=="courses" || $_GET["module"]=="form_courses") { ?>
    <li class="active">
      <a href="?module=courses" style="color:#E4AEE8"><i class="fa fa-graduation-cap"></i>Cursos </a>
      </li>
  <?php
  }

  else { ?>
    <li>
      <a href="?module=courses" style="color:#E4AEE8"><i class="fa fa-graduation-cap"></i> Cursos </a>
      </li>
  <?php
  }

  if ($_GET["module"]=="asignaciones" || $_GET["module"]=="form_asignaciones") { ?>
    <li class="active">
      <a href="?module=courses" style="color:#E4AEE8"><i class="fa fa-handshake-o"></i>Asignacion a cursos </a>
      </li>
  <?php
  }

  else { ?>
    <li>
      <a href="?module=asignaciones" style="color:#E4AEE8"><i class="fa fa-handshake-o"></i> Asignacion a cursos </a>
      </li>
  <?php
  }

  if ($_GET["module"]=="teachers" || $_GET["module"]=="form_teachers") { ?>
    <li class="active">
      <a href="?module=teachers" style="color:#E4AEE8"><i class="fa fa-user"></i>Docentes </a>
      </li>
  <?php
  }

  else { ?>
    <li>
      <a href="?module=teachers" style="color:#E4AEE8"><i class="fa fa-user"></i> Docentes </a>
      </li>
  <?php
  }

	if ($_GET["module"]=="password") { ?>
		<li class="active">
			<a href="?module=password" style="color:#E4AEE8"><i class="fa fa-lock"></i> Cambiar contraseña</a>
		</li>
	<?php
	}

	else { ?>
		<li>
			<a href="?module=password" style="color:#E4AEE8"><i class="fa fa-lock"></i> Cambiar contraseña</a>
		</li>
	<?php
	}
	?>
	</ul>

<?php
}

elseif ($_SESSION['permisos_acceso']=='Gerente') { ?>
	<!-- sidebar menu start -->
    <ul class="sidebar-menu">
        <li class="header">MENU</li>

	<?php

	if ($_GET["module"]=="start") { ?>
		<li class="active" style="color:#E4AEE8">
			<a href="?module=start" ><i class="fa fa-home"></i> Inicio </a>
	  	</li>
	<?php
	}

	else { ?>
		<li>
			<a href="?module=start" style="color:#E4AEE8"><i class="fa fa-home"></i> Inicio </a>
	  	</li>
	<?php
	}


  if ($_GET["module"]=="stock_inventory") { ?>
    <li class="active treeview">
            <a href="javascript:void(0);">
              <i class="fa fa-file-text"></i> <span>Reportes</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
          <ul class="treeview-menu">
            <li class="active"><a href="?module=stock_inventory" style="color:#E4AEE8"><i class="fa fa-circle-o"></i> Stock de Medicamentos</a></li>
            <li><a href="?module=stock_report" style="color:#E4AEE8"><i class="fa fa-circle-o"></i> Registro de medicamentos </a></li>
          </ul>
      </li>
    <?php
  }
  elseif ($_GET["module"]=="stock_report") { ?>
    <li class="active treeview">
            <a href="javascript:void(0);" style="color:#E4AEE8">
              <i class="fa fa-file-text"></i> <span>Reportes</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
          <ul class="treeview-menu">
            <li><a href="?module=stock_inventory"><i class="fa fa-circle-o"></i> Stock de Medicamentos </a></li>
            <li class="active"><a href="?module=stock_report"><i class="fa fa-circle-o"></i> Registro de medicamentos </a></li>
          </ul>
      </li>
    <?php
  }
  else { ?>
    <li class="treeview">
            <a href="javascript:void(0);" style="color:#E4AEE8">
              <i class="fa fa-file-text"></i> <span>Reportes</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
          <ul class="treeview-menu">
            <li><a href="?module=stock_inventory" style="color:#E4AEE8"><i class="fa fa-circle-o"></i>  Stock de Medicamentos </a></li>
            <li><a href="?module=stock_report" style="color:#E4AEE8"><i class="fa fa-circle-o"></i> Registro de medicamentos </a></li>
          </ul>
      </li>
    <?php
  }

	if ($_GET["module"]=="password") { ?>
		<li class="active">
			<a href="?module=password" style="color:#E4AEE8" style="color:#E4AEE8"><i class="fa fa-lock"></i> Cambiar contraseña</a>
		</li>
	<?php
	}
	else { ?>
		<li>
			<a href="?module=password" style="color:#E4AEE8" style="color:#E4AEE8"><i class="fa fa-lock"></i> Cambiar contraseña</a>
		</li>
	<?php
	}
	?>
	</ul>
<?php
}
if ($_SESSION['permisos_acceso']=='Almacen') { ?>

    <ul class="sidebar-menu">
        <li class="header">MENU</li>

	<?php

  if ($_GET["module"]=="start") { ?>
    <li class="active">
      <a href="?module=start" style="color:#E4AEE8"><i class="fa fa-home"></i> Inicio </a>
      </li>
  <?php
  }

  else { ?>
    <li>
      <a href="?module=start" style="color:#E4AEE8"><i class="fa fa-home"></i> Inicio </a>
      </li>
  <?php
  }

  if ($_GET["module"]=="medicines" || $_GET["module"]=="form_medicines") { ?>
    <li class="active">
      <a href="?module=medicines" style="color:#E4AEE8"><i class="fa fa-folder"></i> Datos de medicamentos </a>
      </li>
  <?php
  }
  else { ?>
    <li>
      <a href="?module=medicines" style="color:#E4AEE8"><i class="fa fa-folder"></i> Datos de medicamentos </a>
      </li>
  <?php
  }

  if ($_GET["module"]=="medicines_transaction" || $_GET["module"]=="form_medicines_transaction") { ?>
    <li class="active">
      <a href="?module=medicines_transaction" style="color:#E4AEE8"><i class="fa fa-clone"></i> Registro de medicamentos </a>
      </li>
  <?php
  }
  else { ?>
    <li>
      <a href="?module=medicines_transaction" style="color:#E4AEE8"><i class="fa fa-clone"></i> Registro de medicamentos </a>
      </li>
  <?php
  }

  if ($_GET["module"]=="stock_inventory") { ?>
    <li class="active treeview">
            <a href="javascript:void(0);" style="color:#E4AEE8">
              <i class="fa fa-file-text"></i> <span>Reportes</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
          <ul class="treeview-menu">
            <li class="active"><a href="?module=stock_inventory" style="color:#E4AEE8"><i class="fa fa-circle-o"></i> Stock de Medicamentos </a></li>
            <li><a href="?module=stock_report" style="color:#E4AEE8"><i class="fa fa-circle-o"></i> Registro de medicamentos </a></li>
          </ul>
      </li>
    <?php
  }
  elseif ($_GET["module"]=="stock_report") { ?>
    <li class="active treeview">
            <a href="javascript:void(0);" style="color:#E4AEE8">
              <i class="fa fa-file-text"></i> <span>Reportes</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
          <ul class="treeview-menu">
            <li><a href="?module=stock_inventory" style="color:#E4AEE8"><i class="fa fa-circle-o"></i> Stock de Medicamentos </a></li>
            <li class="active"><a href="?module=stock_report" style="color:#E4AEE8"><i class="fa fa-circle-o"></i> Registro de medicamentos </a></li>
          </ul>
      </li>
    <?php
  }
  else { ?>
    <li class="treeview">
            <a href="javascript:void(0);" style="color:#E4AEE8">
              <i class="fa fa-file-text"></i> <span>Reportes</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
          <ul class="treeview-menu">
            <li><a href="?module=stock_inventory" style="color:#E4AEE8"><i class="fa fa-circle-o"></i> Stock de Medicamentos </a></li>
            <li><a href="?module=stock_report" style="color:#E4AEE8"><i class="fa fa-circle-o"></i> Registro de medicamentos </a></li>
          </ul>
      </li>
    <?php
  }

	if ($_GET["module"]=="password") { ?>
		<li class="active">
			<a href="?module=password" style="color:#E4AEE8"><i class="fa fa-lock"></i> Cambiar contraseña</a>
		</li>
	<?php
	}
	else { ?>
		<li>
			<a href="?module=password" style="color:#E4AEE8"><i class="fa fa-lock"></i> Cambiar contraseña</a>
		</li>
	<?php
	}
	?>
	</ul>
<?php
}
?>
