$("#dataTables1 tbody").on("click", "a#info", function(event){
            var loading = '<p><i class="fa fa-refresh fa-spin"></i> Cargando datos</p>';
            $("#tblDatos").html(loading);
            var id = $(this).data('id');
            $.ajax({
            url: "assets/js/modules/clients/process.php",
            type: "GET",
            data: {
               id: id
            },
            dataType: "JSON",
            success: function (data) {
              if (data.error){
                alert('Cliente no encontrado');
              }
              var estudiante = '<tr><td>' + data.tipo_id + '</td><td>' + data.email + '</td></tr>';
              $("#tblDatos").html(estudiante);
            },
        });

    });
